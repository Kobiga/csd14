package bcas.string.ex;
public class StringOperationDemo {
	public static void main( String args[]) {
		String campus="BCAS";
		String location="jaffna";
		String msg= "welcome    ";
	    String to ="to";
	    String  text1="This is really not immutable";
	    String  text2="This is really not immutable";
	    String  text3="This is really not immutable";
	    String  text4="THIS IS REALLY NOT IMMUTABLE";
	    String  text5="welcome to BCAS ";
	    String  text6="welcome to BCAS ";
	    String  subtext6="hi";
	    
	    
	    StringOperation op =new StringOperation();
	    System.out.println(op.getCharAt(3, campus));
	    System.out.println(op.getConcat(location, campus));
	    System.out.println(op.getUppercase(location));
	    System.out.println(op.getLowrcase(campus));
	    System.out.println(op.getEndsWith(text1,"immu"));
	    System.out.println(op.gettrim(msg));
	    System.out.println(op.getCharArray(msg));
	    System.out.println(op.getequal(text1,text2));
	    System.out.println(op.getequal(text1,text4));
	    System.out.println(op.getequalignore(text1,text4));
	    System.out.println(op.getindexof(text1,'o'));
	    System.out.println(op.getLastindexof(text1,'o'));
	    System.out.println(op.getindexof(text6,'e',5));
	    System.out.println(op.getLastindexof(text6,'e',5));
	    System.out.println(op.getindexof(text1,text2));
	    System.out.println(op.getLastindexof(text3,text4));
	    System.out.println(op.getlength(to));
	    System.out.println(op.getreplace("to",'t','o'));
	    System.out.println(op.getSubString(text1,9));
	    System.out.println(op.getSubString(text1,9,12));
	    
	}
	    
	    
	}


