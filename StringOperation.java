package bcas.string.ex;


public class StringOperation {

	
	public char getCharAt(int index, String word) {
		char ch= word.charAt(index);
		return ch;
	}
	
	public String getConcat( String word1,String word2) {
		String str= word1.concat(word2);
		return str;
		
	}
	public String getUppercase( String word) {
		String str= word.toUpperCase();
		return str;
	
	}
	
	public String getLowrcase( String word) {
		String str= word.toLowerCase();
		return str;
	}
	
	public boolean getEndsWith( String text1,String word) {
		boolean bo= text1.endsWith(word);
		return bo;
		
	
}
	public String gettrim (String word) {
		String str= word.trim();
		return str;

	
	}
	public char[] getCharArray (String word) {
		char[] ch= word.toCharArray();
		return ch;

}
	public boolean getequal (String text1, String text2) {
		boolean bo= text1.equals(text2);
		return bo;
	}
	public boolean getequalignore (String text1, String text2) {
		boolean bo= text1.equalsIgnoreCase(text2);
		return bo;
	}

	public int getindexof (String text1, char word) {
		int in= text1.indexOf(word);
		return in;
	}
	public int getLastindexof (String text1, char word) {
		int in= text1.lastIndexOf(word);
		return in;
	
	}
	public int getindexof (String text1, char word, int index) {
		int in= text1.indexOf(word, index);
		return in;
	}
	public int getLastindexof (String text1, char word, int index) {
		int in= text1.lastIndexOf(word, index);
		return in;
	}

	public int getindexof (String text1, String text2) {
	int in= text1.indexOf(text2);
	return in;
	}
	public int getLastindexof (String text1, String text2) {
		int in= text1.lastIndexOf(text2);
		return in;
}
	public int getlength (String word) {
		int in= word.length();
		return in;
}
	public String getreplace ( String text,char word,char  word2) {
		String str= text.replace(word, word2);
		return str;
}
	public String getSubString (String text, int index) {
		String str= text.substring(9);
		return str;
	}
	public String getSubString (String text, int index1,int index2) {
		String str= text.substring(9,12);
		return str;
	}
}